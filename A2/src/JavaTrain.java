import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int fahrzeit = 0;
		char haltInSpandau = 'n';
		char richtungHamburg = 'n';
		char haltInStendal = 'j';
		char endetIn = 'h';

		/*
		System.out.println("W�hlen Sie ihr Fahrtziel:");
		endetIn = scan.next(".").charAt(0); 
		*/
		
		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau

		if (haltInSpandau == 'j')
		{
			fahrzeit = fahrzeit + 2; // Halt in Spandau
		}
		
		if (richtungHamburg == 'n') // Fahrt Richtung Hamburg
		{
			fahrzeit = fahrzeit + 34;
			
			if (haltInStendal == 'j') // Halt in Stendal
			{
				fahrzeit = fahrzeit + 10;
			}
			
			fahrzeit = fahrzeit + 6; //Bei Halt in Stendal: 16 Min, sonst nur 10 Min
			
			if (endetIn == 'h') // Ende in Hannover
			{
				fahrzeit = fahrzeit + 62;
				System.out.printf("Sie erreichen Hannover nach %d Minuten. Wir w�nschen Ihnnen eine sch�ne Fahrt!", fahrzeit);
			}
			else if (endetIn == 'w') // Ende in Wolfsburg
			{
				fahrzeit = fahrzeit + 29;
				System.out.printf("Sie erreichen Wolfsburg nach %d Minuten. Wir w�nschen Ihnnen eine sch�ne Fahrt!", fahrzeit);
			}
			else if (endetIn == 'b') // Ende in Braunschweig
			{
				fahrzeit = fahrzeit + 50;
				System.out.printf("Sie erreichen Braunschweig nach %d Minuten. Wir w�nschen Ihnnen eine sch�ne Fahrt!", fahrzeit);
			}
		}
		else
		{
			fahrzeit = fahrzeit + 96; // Ende in Hamburg
			System.out.printf("Sie erreichen Hamburg nach %d Minuten. Wir w�nschen Ihnnen eine sch�ne Fahrt!", fahrzeit);
		}
		scan.close();
	}
}
