import java.util.Scanner;

public class Rabattsystem
{
	public static void main(String[] args)
	{
		//Deklarationen
		Scanner scan = new Scanner(System.in);
		double verkaufspreis;
		double endpreis;
		double rabatt;
		double preisrabatt;
		
		//Eingabe
		System.out.println("Bestellwert: ");
		verkaufspreis = scan.nextDouble();
		
		// Rechnungen:
		// Bei einem Verkaufspreis zwischen 0 und 100
		if (verkaufspreis <= 100 && verkaufspreis >= 0) 
		{
			rabatt = 10;
			preisrabatt = (verkaufspreis * 10) / 100;
			endpreis = verkaufspreis - preisrabatt;
		}
		// Bei einem Verkaufspreis zwischen 1000 und 500
		else if (verkaufspreis >= 100 && verkaufspreis <= 500) 
		{
			rabatt = 15;
			preisrabatt = (verkaufspreis * 15) / 100;
			endpreis = verkaufspreis - preisrabatt;
		}
		// Bei einem Verkaufspreis �ber 500
		else 
		{
			rabatt = 20;
			preisrabatt = (verkaufspreis * 20) / 100;
			endpreis = verkaufspreis - preisrabatt;
		}
		
		//Ausgaben
		System.out.printf("Ihr Preis (ohne Rabatt): %.2f Euro\n", verkaufspreis);
		System.out.printf("Ihr Rabatt: %.2f Prozent\n" , rabatt);
		System.out.printf("Ihr Endpreis: %.2f Euro", endpreis);
		scan.close();
	}
}
