import java.util.Scanner;

public class PCHaendler {
	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {
		
		String artikel = null;
		liesString(artikel);
		int anzahl = 0;
		liesInt(anzahl);

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}
	public static String liesString(String text) {
		System.out.println("Was möchten Sie bestellen?");
		return text = myScanner.next();
	}
	public static int liesInt(int anzahl) {
		System.out.println("Geben Sie die Anzahl ein:");
		return anzahl = myScanner.nextInt();
	}
	


}