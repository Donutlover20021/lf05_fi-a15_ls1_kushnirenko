import java.util.Locale;
import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
    public static void main(String[] args)
    {
    	Locale.setDefault(new Locale("en", "US"));
    	String fortsetzen;
    	
    	do
    	{
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen(), rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        	fahrkartenAusgeben();
        	rueckgeldAusgeben(rueckgabebetrag);
        	
        	System.out.println("Willst du deine Bestellung fortsetzen?");
        	fortsetzen = tastatur.next();        	
    	}while ( fortsetzen.contains("j") || fortsetzen.contains("y"));
    	
    	tastatur.close();
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    
    public static double fahrkartenbestellungErfassen () 
    {
			// Die Fahrkarten als Arrays deklarieren
			String[] Kartenbez = new String [] 
	    			{
	    					"Einzelfahrschein Berlin AB ",
	    					"Einzelfahrschein Berlin BC",
	    					"Einzelfahrschein Berlin ABC",
	    					"Kurzstrecke",
	    					"Tageskarte Berlin AB",
	    					"Tageskarte Berlin BC",
	    					"Tageskarte Berlin ABC",
	    					"Kleingruppen-Tageskarte Berlin AB",
	    					"Kleingruppen-Tageskarte Berlin BC",
	    					"Kleingruppen-Tageskarte Berlin ABC"
	    			};
	    	double[] Kartenpr  = new double [] {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
	    	
	    	// Den Betrag aussuchen
	    	System.out.println("W�hlen Sie ihre Wunschfahrkarte aus: ");
	    	for (int i = 0; i < Kartenbez.length; i++) 
	    	{ 
	    		int j = i + 1;
	    		System.out.printf("\n[" + j + "] " + Kartenbez[i] + " | %.2f Euro", Kartenpr[i]);
	    	}
	    	System.out.println();
	    	int Kartenwahl = tastatur.nextInt();
	    	
	    	while (Kartenwahl <= 0 || Kartenwahl > Kartenbez.length) 
	    	{
	    		System.out.println(">>>>Die Ticketnummer ist ung�ltig!");
	    		System.out.println("W�hlen Sie ihre Wunschfahrkarte aus: ");
		    	Kartenwahl = tastatur.nextInt();
	    	}
	    	
	       //Die Anzahl der Karten aussuchen     	
	        System.out.println("W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus:");
	        int Anzahl = tastatur.nextInt(); 
	        
	        while (Anzahl > 10 || Anzahl < 1)
	        {
	        	System.out.println(">>>>Die Anzahl der Tickets ist ung�ltig!");
	        	System.out.println("W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus:");
		        Anzahl = tastatur.nextInt(); 
	        }
	        return Kartenpr[Kartenwahl - 1] * Anzahl;
    }
    
    public static double fahrkartenBezahlen (double zuZahlenderBetrag) 
    {
    	// Geldeinwurf
    	double eingezahlterGesamtbetrag = 0.00;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //Formatierung des Doubles
        	System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	    double eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben () 
    {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try 
           {
 			Thread.sleep(250);
 		}
           catch (InterruptedException e) 
           {
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben (double rueckgabebetrag) 
    {
        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        if(rueckgabebetrag > 0.0)
        {
     	 //Formatierung des Doubles
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f ", rueckgabebetrag);
     	   System.out.println("EURO wird in folgenden M�nzen ausgezahlt:");
     	   
            while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
         	 rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
         	 rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
         	 rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
         	 rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
         	 rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
         	 rueckgabebetrag -= 0.05;
            }
        }
    }
}
