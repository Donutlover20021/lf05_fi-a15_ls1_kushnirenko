import java.util.Scanner;

public class Rechner {

	public static void main(String[] args) 
	{
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		 int zahl1 = myScanner.nextInt();

		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 int zahl2 = myScanner.nextInt();
		 
		 System.out.print("Bitte geben Sie eine Rechenart ein (+, -, *, /) : ");
		 String op = myScanner.next();
		 
		 switch(op) {
		 case "+":
			 System.out.print("\nErgebnis der Addition lautet: ");
			 System.out.print(zahl1 + " + " + zahl2 + " = " + zahl1 + zahl2);
			 break;
		 case "-":
			 int Erg = zahl1 - zahl2;
			 System.out.print("\nErgebnis der Subtraktion lautet: ");
			 System.out.print(zahl1 + " - " + zahl2 + " = " + Erg);
			 break;
		 case "*":
			 System.out.print("\nErgebnis der Multiplikation lautet: ");
			 System.out.print(zahl1 + " * " + zahl2 + " = " + zahl1 * zahl2);
			 break;
		 case "/":
			 System.out.print("\nErgebnis der Division lautet: ");
			 System.out.print(zahl1 + " / " + zahl2 + " = " + zahl1 / zahl2);
			 break;
		 }
		 myScanner.close();
	}
}
