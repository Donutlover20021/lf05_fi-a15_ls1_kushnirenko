import java.util.ArrayList;

public class Raumschiff 
{
	//Attributen
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	//Konstruktoren
	public Raumschiff() 
	{
		
	}
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent, int zustandHuelleInProzent,
			int zustandLebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) 
	{
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchildeInProzent(zustandSchildeInProzent);
		setHuelleInProzent(zustandHuelleInProzent);
		setLebenserhaltungssystemeInProzent(zustandLebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		setSchiffsname(schiffsname);
	}
	
	//getter und setter
	public int getPhotonentorpedoAnzahl()
	 {
	 return this.photonentorpedoAnzahl;
	 }
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu)
	 {
	 this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	 }
	
	public int getEnergieversorgungInProzent()
	 {
	 return this.energieversorgungInProzent;
	 }
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu)
	 {
	 this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	 }
	
	public int getSchildeInProzent()
	 {
	 return this.schildeInProzent;
	 }
	public void setSchildeInProzent(int zustandSchildeInProzentNeu)
	 {
	 this.schildeInProzent = zustandSchildeInProzentNeu;
	 }
	
	public int getHuelleInProzent()
	 {
	 return this.huelleInProzent;
	 }
	public void setHuelleInProzent(int zustandHuelleInProzentNeu)
	 {
	 this.huelleInProzent = zustandHuelleInProzentNeu;
	 }
	
	public int getLebenserhaltungssystemeInProzent()
	 {
	 return this.lebenserhaltungssystemeInProzent;
	 }
	public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu)
	 {
	 this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzentNeu;
	 }
	
	public int getAndroidenAnzahl()
	 {
	 return this.androidenAnzahl;
	 }
	public void setAndroidenAnzahl(int androidenAnzahl)
	 {
	 this.androidenAnzahl = androidenAnzahl;
	 }
	
	public String getSchiffsname()
	 {
	 return this.schiffsname;
	 }
	public void setSchiffsname(String schiffsname)
	 {
	 this.schiffsname = schiffsname;
	 }
	
	public void addLadung(Ladung neueLadung) 
	{
		//was ich mir unter dieser Methode vorstellen kann
		ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) 
	{
		
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) 
	{
		
	}
	
	private void treffer(Raumschiff r) 
	{
		
	}
	
	public void nachrichtAnAlle(String Message) 
	{
		
	}
	
	public void zustandRaumschiff() 
	{
		
	}
	
	public void ladungsverzeichnisAusgeben() 
	{
		
	}
}


