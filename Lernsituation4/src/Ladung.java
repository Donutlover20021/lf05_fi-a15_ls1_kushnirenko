public class Ladung 
{
	//Attributen
	private String bezeichnung;
	private int menge;
	
	//Konstruktoren
	public Ladung() 
	{
		
	}
	public Ladung(String bezeichnung, int menge) 
	{
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}
	
	//getter und setter
	public String getBezeichnung()
	 {
	 return this.bezeichnung;
	 }
	public void setBezeichnung(String name) 
	{
		this.bezeichnung = name;
	}
	
	public int getMenge()
	 {
	 return this.menge;
	 }
	public void setMenge(int menge)
	 {
	 this.menge = menge;
	 }
	
	public String toString() 
	{
		return bezeichnung + menge;
	}
	
}
