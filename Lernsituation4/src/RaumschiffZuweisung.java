
public class RaumschiffZuweisung {

	public static void main(String[] args) 
	{
		//Deklarierung aller Raumschiffe
				Raumschiff klingonen = new Raumschiff();
				Raumschiff romulaner = new Raumschiff();
				Raumschiff vulkanier = new Raumschiff();
				Ladung fer = new Ladung("Ferengi Schneckensaft", 200);
				Ladung bat = new Ladung("Bat'leth Klingonen Schwert", 200);
				Ladung borg = new Ladung("Borg-Schrott", 5);
				Ladung rot = new Ladung("Rote Materie", 2);
				Ladung plasma = new Ladung("Plasma-Waffe", 50);
				Ladung forsch = new Ladung("Forschungssonde", 35);
				Ladung photon = new Ladung("Photonentorpedo", 3);
				
				//Zuweisung der Werte unter "klingonen" (und deren Ladungen)
				klingonen.setPhotonentorpedoAnzahl(1);
				klingonen.setEnergieversorgungInProzent(100);
				klingonen.setSchildeInProzent(100);
				klingonen.setHuelleInProzent(100);
				klingonen.setLebenserhaltungssystemeInProzent(100);
				klingonen.setSchiffsname("IKS Hegh'ta");
				klingonen.setAndroidenAnzahl(2);
				klingonen.addLadung(fer);
				klingonen.addLadung(bat);
				
				//Zuweisung der Werte unter "romulaner" (und deren Ladungen)
				romulaner.setPhotonentorpedoAnzahl(2);
				romulaner.setEnergieversorgungInProzent(100);
				romulaner.setSchildeInProzent(100);
				romulaner.setHuelleInProzent(100);
				romulaner.setLebenserhaltungssystemeInProzent(100);
				romulaner.setSchiffsname("IRW Khazara");
				romulaner.setAndroidenAnzahl(2);
				klingonen.addLadung(borg);
				klingonen.addLadung(rot);
				klingonen.addLadung(plasma);
				
				//Zuweisung der Werte unter "vulkanier" (und deren Ladungen)
				vulkanier.setPhotonentorpedoAnzahl(0);
				vulkanier.setEnergieversorgungInProzent(80);
				vulkanier.setSchildeInProzent(80);
				vulkanier.setHuelleInProzent(50);
				vulkanier.setLebenserhaltungssystemeInProzent(100);
				vulkanier.setSchiffsname("Ni'Var");
				vulkanier.setAndroidenAnzahl(5);
				klingonen.addLadung(forsch);
				klingonen.addLadung(photon);
	}

}
