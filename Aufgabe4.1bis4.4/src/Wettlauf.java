public class Wettlauf {
	//Quelle: Aufgaben zu Schleifen 2 Aufgabe 7
	public static void main(String[] args) {
		int sek = 0;
		//Geschwindigkeiten der Sprinter
		float speedA = 9.5f;
		float speedB = 7;
		//Meter, die zugelegt wurden
		float metresA = 0;
		float metresB = 0;
		//Tabellenkopf
		System.out.println("  Sekunden  | Sprinter A | Sprinter B ");
		System.out.println("--------------------------------------");
		//Tabelle + Berechnung
		do
		{
			metresA = speedA * sek;
			metresB = speedB * sek + 250;
			System.out.printf("%12d| %11.1f| %11.1f \n", sek, metresA, metresB);
			sek++;
		}
		//Ich habe mich dazu entschieden, die Zielentfernung 1000.9 zu machen,
		//damit bei der Tabelle auch 1000 steht
		while (metresA <= 1000.9 & metresB <= 1000.9 );
	}
}
