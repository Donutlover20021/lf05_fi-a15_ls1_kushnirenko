import java.util.Scanner;
//Quelle: Aufgaben zu Verzweigungen Aufgabe 5
public class BMIrechner {
	public static void main(String[] args) {
		//Deklerationen
		Scanner scan = new Scanner(System.in);
		int gewicht;
		double gr��e;
		double bmi;
		char geschlecht;
		
		//Eingaben
		System.out.println("K�rpergr��e(in m): ");
		gr��e = scan.nextDouble();
		
		System.out.println("K�rpergewicht(in kg): ");
		gewicht = scan.nextInt();
		
		System.out.println("Ihr Geschlecht: ");
		geschlecht = scan.next().toUpperCase().charAt(0);
		
		//Rechnung und Ausgabe des BMI
		bmi = gewicht / (gr��e * gr��e);
		System.out.printf("Ihr BMI: %.2f", bmi);
		
		//Klassifikation des BMI und Ausgabe der Klassifikation
		if (geschlecht == 'W') 
		{
			if (bmi < 19) 
			{
				System.out.print("\nSie befinden sich im Untergewicht.");
			}
			else if (bmi >=19 || bmi <= 24) 
			{
				System.out.print("\nSie befinden sich im Normalgewicht.");
			}
			else 
			{
				System.out.print("\nSie befinden sich im �bergewicht.");
			}
		}
		else if(geschlecht == 'M')
		{
			if (bmi < 20) 
			{
				System.out.print("\nSie befinden sich im Untergewicht.");
			}
			else if (bmi >=20 || bmi <= 25) 
			{
				System.out.print("\nSie befinden sich im Normalgewicht.");
			}
			else 
			{
				System.out.print("\nSie befinden sich im �bergewicht.");
			}
		}
		scan.close();
	}
}

