import java.util.Scanner;
//Quelle: Aufgaben zu Schleifen 1 Aufgabe 4
public class Folgen {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		//Aussuchen von der Aufgabe
		System.out.println("Such dir eine der folgenden Aufgaben aus (a, b, c, d, e): ");
        char aufgabe = scan.next().toUpperCase().charAt(0);
        //For-Schleifen mit Ausgabe
        switch (aufgabe) 
        {
        case 'A':
        	for (byte i = 99 ; i>=9; i-=3) 
        	{
        		System.out.println(i);
        	}
        	break;
        case 'B':
        	int b = 3;
    		for (int i = 1 ; i<=400; i+=b) 
        	{
    			System.out.println(i);
    			if (i != 1) 
    			{
    				b+=2;
    			}
        	}
        	break;
        case 'C':
        	for (byte i = 2 ; i<=102; i+=4) 
        	{
        		System.out.println(i);
        	}
        	break;
        case 'D':
        	int d = 12;
    		for (int i = 4 ; i<=1024; i+=d) 
        	{
    			System.out.println(i);
    			if (i != 4) 
    			{
    				d+=8;
    			}
        	}
        	break;
        case 'E':
        	for (int i = 2 ; i<=32768; i*=2) 
        	{
        		System.out.println(i);
        	}
        	break;
        default:
        	//Falls was falsches eingegeben wird
        	System.out.print("Du hast etwas falsches eingegeben.");
        	break;
        }
        scan.close();
	}

}
